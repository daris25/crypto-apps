import moment from 'moment';
import numbro from 'numbro';
import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import styles from './styles';

export default function DetailCrypt(props) {
  const {
    route: { params },
    navigation,
  } = props;

  const renderHeader = () => {
    return (
      <View style={styles.header.container}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <AntDesign name="arrowleft" size={25} color="white" />
        </TouchableOpacity>
        <Text style={styles.header.textTitle}>{params?.item?.symbol}</Text>
      </View>
    );
  };

  const renderMain = () => {
    return (
      <ScrollView style={styles.content.container}>
        {renderHeaderBox()}
        {renderLastUpdate()}
        {renderLastQuotes(
          'Last 24 Hours',
          params?.item?.quote?.USD?.percent_change_24h,
        )}
        {renderLastQuotes(
          'Last 7 Days',
          params?.item?.quote?.USD?.percent_change_7d,
        )}
        {renderLastQuotes(
          'Last 30 Days',
          params?.item?.quote?.USD?.percent_change_30d,
        )}
        {renderLastQuotes(
          'Last 60 Days',
          params?.item?.quote?.USD?.percent_change_60d,
        )}
        {renderLastQuotes(
          'Last 90 Days',
          params?.item?.quote?.USD?.percent_change_90d,
        )}
      </ScrollView>
    );
  };

  const renderHeaderBox = () => {
    return (
      <View style={styles.content.boxHeader}>
        <View>
          <Text style={styles.content.textNameHeader}>
            {params?.item?.name}
          </Text>
          <Text style={styles.content.textPrice}>
            $
            {numbro(params?.item?.quote?.USD?.price).format({
              thousandSeparated: true,
              mantissa: 2,
            })}
          </Text>
          <View style={styles.content.viewMarket}>
            <Text style={styles.content.titleBoxPadding}>Market Cap</Text>
            <Text style={styles.content.subTitleBoxPadding}>
              $
              {numbro(params?.item?.quote?.USD?.market_cap).format({
                thousandSeparated: true,
                mantissa: 0,
              })}
            </Text>
          </View>
        </View>
        <View>
          <View
            style={colorBadges(params?.item?.quote?.USD?.percent_change_24h)}>
            <Text style={styles.content.text24H}>
              {params?.item?.quote?.USD?.percent_change_24h.toFixed(2)}%
            </Text>
          </View>
        </View>
      </View>
    );
  };

  const renderLastUpdate = () => {
    return (
      <View style={styles.content.lasUpdate}>
        <Text style={styles.content.titleBoxPadding}>Last Update</Text>
        <Text style={styles.content.subTitleBoxPadding}>
          {moment(params?.item?.last_updated).format('DD MMMM YYYY HH:mm')}
        </Text>
      </View>
    );
  };

  const renderLastQuotes = (title, quote) => {
    return (
      <View style={styles.content.viewQuotes}>
        <Text style={styles.content.titleQuotes}>{title}</Text>
        <View style={[colorBadges(quote), { paddingVertical: 5 }]}>
          <Text style={styles.content.textQuotes}>{quote.toFixed(2)}%</Text>
        </View>
      </View>
    );
  };

  return (
    <SafeAreaView
      style={{ backgroundColor: '#403e45', width: '100%', height: '100%' }}>
      {renderHeader()}
      {renderMain()}
    </SafeAreaView>
  );
}

const colorBadges = val => {
  let style = {
    padding: 10,
    backgroundColor: 'black',
    borderRadius: 6,
  };
  if (val > 0) {
    style.backgroundColor = 'green';
  } else {
    style.backgroundColor = 'maroon';
  }
  return style;
};
