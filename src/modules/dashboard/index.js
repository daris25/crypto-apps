import numbro from 'numbro';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  ActivityIndicator,
  FlatList,
  RefreshControl,
} from 'react-native';
import { ListItem } from '../../component/card';
import { listingsLatest } from '../../config/rest-api/index';
import styles from './styles';

export default function Dashboard(props) {
  const { navigation } = props;

  const limit = 25;
  const [offset, setOffset] = useState(1);
  const [listItem, setListItem] = useState([]);
  const [refreshing, setRefreshing] = useState(false);
  const [refreshFetch, setRefreshFetch] = useState(false);

  useEffect(() => {
    fetchApi();
  }, [fetchApi]);

  const fetchApi = useCallback(async () => {
    const latest = await listingsLatest({
      start: offset,
      limit,
    });
    if (latest?.data.length > 0) {
      setListItem([...listItem, ...latest?.data]);
      setOffset(offset + latest?.data.length);
    }
    setRefreshFetch(false);
    setRefreshing(false);
  }, [listItem, offset]);

  const formatCurrency = (val, mantissa) => {
    return numbro(val).format({
      thousandSeparated: true,
      mantissa: mantissa ?? 2,
    });
  };

  const onRefresh = useCallback(() => {
    setRefreshing(true);
    fetchApi();
  }, [fetchApi]);

  const onReachedList = ({ distanceFromEnd }) => {
    if (distanceFromEnd) {
      console.log('trueRes', distanceFromEnd);
      setRefreshFetch(true);
      fetchApi();
    }
  };

  const renderFlatList = () => {
    const renderItem = ({ item }) => (
      <ListItem
        title={item.symbol}
        subTitle={item.name}
        price={formatCurrency(item?.quote?.USD?.price)}
        percentOne={item?.quote?.USD?.percent_change_24h.toFixed(2)}
        percentTwo={item?.quote?.USD?.percent_change_7d.toFixed(2)}
        percentThree={item?.quote?.USD?.percent_change_30d.toFixed(2)}
        onPress={() => navigation.navigate('DetailCrypt', { item })}
      />
    );
    return (
      <View style={styles.content.paddingContainer}>
        <FlatList
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={() => onRefresh()}
            />
          }
          onEndReached={onReachedList}
          onEndReachedThreshold={0.1}
          data={listItem}
          renderItem={renderItem}
          keyExtractor={item => item.id}
        />
      </View>
    );
  };

  const renderHeader = () => {
    return (
      <View style={styles.header.container}>
        <View>
          <Text style={{ fontSize: 25, color: 'white' }}>
            Current Portofolio
          </Text>
          <Text style={styles.header.textHeader}>BALANCE</Text>
          <Text style={styles.header.textSubtitle}>$100.24</Text>
        </View>
        {/* <TouchableOpacity style={styles.header.btnLogut}>
          <Text style={styles.header.textBtn}>Logout</Text>
        </TouchableOpacity> */}
      </View>
    );
  };

  const renderLoadingFetch = useMemo(() => {
    return refreshFetch === true ? (
      <View style={{ height: 40 }}>
        <ActivityIndicator size="large" color="white" />
      </View>
    ) : null;
  }, [refreshFetch]);

  return (
    <SafeAreaView>
      <View style={styles.container}>
        {renderHeader()}
        {renderLoadingFetch}
        {renderFlatList()}
      </View>
    </SafeAreaView>
  );
}
