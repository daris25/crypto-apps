export default {
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: '#332f3d',
  },
  header: {
    container: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingVertical: 15,
      paddingHorizontal: 15,
      // backgroundColor: '#5F939A',
      alignItems: 'center',
    },
    textHeader: {
      fontSize: 15,
      color: 'white',
      marginTop: 15,
      // fontWeight: 'bold',
    },
    textSubtitle: {
      fontWeight: 'bold',
      fontSize: 25,
      color: 'white',
    },
    btnLogut: {
      paddingHorizontal: 15,
      paddingVertical: 10,
      backgroundColor: '#F2EDD7',
      borderRadius: 8,
    },
    textBtn: {
      color: 'black',
      fontSize: 15,
      fontWeight: 'bold',
    },
  },
  content: {
    paddingContainer: { padding: 10 },
  },
};
