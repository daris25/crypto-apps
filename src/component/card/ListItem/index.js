import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

export default function ListCard(props) {
  const {
    title,
    subTitle,
    price,
    percentOne,
    percentTwo,
    percentThree,
    onPress,
  } = props;

  const renderColor = val => {
    let color = 'black';
    if (val > 0) {
      color = 'green';
    } else {
      color = 'maroon';
    }

    return color;
  };

  return (
    <TouchableOpacity onPress={onPress} style={styles.container}>
      <View style={styles.flexName}>
        <Text style={styles.textTitle}>{title ?? 'NNn'}</Text>
        <Text style={styles.textName}>{subTitle ?? 'None Name'}</Text>
      </View>
      <View style={styles.flexPriceGrid}>
        <View style={styles.flexPrice}>
          <Text style={styles.textHeaderPrice}>24h %</Text>
          <Text
            style={[
              styles.textHeaderPrice,
              { color: renderColor(percentOne), fontWeight: 'bold' },
            ]}>
            {percentOne ?? '$1.000'}%
          </Text>
        </View>
        <View style={styles.flexPrice}>
          <Text style={styles.textHeaderPrice}>7d %</Text>
          <Text
            style={[
              styles.textHeaderPrice,
              { color: renderColor(percentTwo), fontWeight: 'bold' },
            ]}>
            {percentTwo ?? '$1.000'}%
          </Text>
        </View>
        <View style={styles.flexPrice}>
          <Text style={styles.textHeaderPrice}>30d %</Text>
          <Text
            style={[
              styles.textHeaderPrice,
              { color: renderColor(percentThree), fontWeight: 'bold' },
            ]}>
            {percentThree ?? '$1.000'}%
          </Text>
        </View>
        <View style={styles.flexPrice}>
          <Text style={styles.textHeaderPrice}>Price</Text>
          <Text
            style={[
              styles.textHeaderPrice,
              { color: renderColor(price), fontWeight: 'bold' },
            ]}>
            ${price ?? '$1.000'}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
}

const styles = {
  container: {
    width: '100%',
    // height: 55,
    backgroundColor: '#DEEDF0',
    marginBottom: 10,
    paddingHorizontal: 10,
    paddingVertical: 5,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderRadius: 6,
  },
  flexName: {
    // flexDirection: 'row',
    width: '25%',
  },
  textTitle: {
    fontWeight: 'bold',
    fontSize: 16,
    color: 'black',
  },
  textName: {
    // marginLeft: 10,
    // fontWeight: 'bold',
    fontSize: 13,
    textTransform: 'capitalize',
  },
  flexPrice: {
    alignItems: 'center',
  },
  textHeaderPrice: {
    // fontWeight: 'bold',
    color: 'black',
    fontSize: 13,
  },
  flexPriceGrid: {
    flexDirection: 'row',
    width: '70%',
    justifyContent: 'space-between',
  },
};
