import { apiCrypto } from '../config';

export const listingsLatest = async params => {
  const get = await fetch(
    `${apiCrypto}/cryptocurrency/listings/latest?start=${params.start}&limit=${params.limit}&convert=USD`,
    {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'X-CMC_PRO_API_KEY': '48760a9d-b644-4440-85dc-f5e72c48d3ae',
      },
    },
  );

  console.log('RESPONSE >>', get.valueOf());
  return await get.json();
};
