import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { Dashboard, DetailCrypt } from './src/modules/index';

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator headerMode={false}>
        <Stack.Screen name="Dashboard" component={Dashboard} />
        <Stack.Screen name="DetailCrypt" component={DetailCrypt} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
